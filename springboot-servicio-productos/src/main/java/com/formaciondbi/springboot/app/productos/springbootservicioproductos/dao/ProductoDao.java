package com.formaciondbi.springboot.app.productos.springbootservicioproductos.dao;

import com.formaciondbi.springboot.app.productos.springbootservicioproductos.models.entity.Producto;

import org.springframework.data.repository.CrudRepository;

public interface ProductoDao extends CrudRepository<Producto, Long> {}
