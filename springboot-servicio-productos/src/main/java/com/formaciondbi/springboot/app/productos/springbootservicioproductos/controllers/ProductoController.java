package com.formaciondbi.springboot.app.productos.springbootservicioproductos.controllers;

import java.util.List;

import com.formaciondbi.springboot.app.productos.springbootservicioproductos.models.entity.Producto;
import com.formaciondbi.springboot.app.productos.springbootservicioproductos.models.service.IProductoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductoController {

  @Autowired
  private IProductoService iProductoService;

  @GetMapping("/listar")
  public List<Producto> listar() {
    return iProductoService.findAll();
  }

  @GetMapping("/error")
  public String error() {
    return "Error";
  }

  @GetMapping("/ver/{id}")
  public Producto detalle(@PathVariable Long id) {
    return iProductoService.findById(id);
  }
}
