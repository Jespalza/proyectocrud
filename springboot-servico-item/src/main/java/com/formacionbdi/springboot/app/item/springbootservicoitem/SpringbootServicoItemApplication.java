package com.formacionbdi.springboot.app.item.springbootservicoitem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class SpringbootServicoItemApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringbootServicoItemApplication.class, args);
  }
}
