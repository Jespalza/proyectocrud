package com.formacionbdi.springboot.app.item.springbootservicoitem.service;

import java.util.List;

import com.formacionbdi.springboot.app.item.springbootservicoitem.models.Item;

public interface IItemService {
    public List<Item> findAll();
    public Item findById(Long id, Integer cantidad);
    
}
