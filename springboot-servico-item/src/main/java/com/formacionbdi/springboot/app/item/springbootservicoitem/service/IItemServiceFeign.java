package com.formacionbdi.springboot.app.item.springbootservicoitem.service;

import com.formacionbdi.springboot.app.item.springbootservicoitem.clients.ProductoClienteRest;
import com.formacionbdi.springboot.app.item.springbootservicoitem.models.Item;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service("serviceFeign")
@Primary
public class IItemServiceFeign implements IItemService {

  @Autowired
  private ProductoClienteRest clienteFeign;

  @Override
  public List<Item> findAll() {
    return clienteFeign
      .listar()
      .stream()
      .map(p -> new Item(p, 1))
      .collect(Collectors.toList());
  }

  @Override
  public Item findById(Long id, Integer cantidad) {
    return new Item(clienteFeign.detalle(id), cantidad);
  }
}
