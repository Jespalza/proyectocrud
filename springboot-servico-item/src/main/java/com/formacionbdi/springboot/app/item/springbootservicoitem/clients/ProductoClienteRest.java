package com.formacionbdi.springboot.app.item.springbootservicoitem.clients;

import com.formacionbdi.springboot.app.item.springbootservicoitem.models.Product;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "servicio-productos", url = "localhost:8001")
public interface ProductoClienteRest {
  @GetMapping("/listar")
  public List<Product> listar();

  @GetMapping("/ver/{id}")
  public Product detalle(@PathVariable Long id);
}
